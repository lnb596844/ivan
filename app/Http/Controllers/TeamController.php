<?php

namespace App\Http\Controllers;

use App\Team;
use App\Grade;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the teams.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Team::orderBy('name')->get(['id', 'name']);
    }

    /**
     * Display the specified team with grades.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id Team ID
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'data' => Team::with('grades')->where('id', $id)->first()
        ]);
    }

    /**
     * Update the specified team with grades in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id Team ID
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'team' => 'required|exists:teams,id',
            'objective' => 'required|string|max:255',
        ]);

        $grade = Grade::updateOrCreate(['team_id' => $request->team], [
            'objective' => $request->objective,
            'grades' => $request->grades,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Data saved',
            'data' => $grade
        ]);
    }
}
