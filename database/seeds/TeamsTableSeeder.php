<?php

use App\Team;
use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * List of teams.
     *
     * @var string
     */
    protected $teams = '[
        {
            "id": 1,
            "name": "Team1",
            "created_at": "2018-11-24 18:24:37",
            "updated_at": "2018-11-24 18:24:37",
            "deleted_at": null
        },
        {
            "id": 2,
            "name": "Team2",
            "created_at": "2018-11-24 18:24:37",
            "updated_at": "2018-11-24 18:24:37",
            "deleted_at": null
        },
        {
            "id": 3,
            "name": "Team3",
            "created_at": "2018-11-24 18:24:37",
            "updated_at": "2018-11-24 18:24:37",
            "deleted_at": null
        }
    ]';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (json_decode($this->teams) as $team) {
            Team::create([
                'id' => $team->id,
                'name' => $team->name,
                'created_at' => $team->created_at,
                'updated_at' => $team->updated_at,
                'deleted_at' => $team->deleted_at,
            ]);
        }
    }
}
