Models:
    - app/Grade.php
    - app/Team.php

Controlelrs:
    - app/Http/Controlelrs/HomeController.php
    - app/Http/Controlelrs/TeamController.php

Routes:
    - routes/web.php

resources:
    JS:
        - all files from 
            resources/js/*
    CSS:
        - resources/sass/app.scss

    Views: 
        - resources/views/home.blade.php
        - resources/views/layouts/app.blade.php

Database:
    teams_table:
        id(integer),
        name(varchar),
        created_at(date),
        updated_at(date),
        deleted_at(date)

    grades:
        id(integer),
        team_id(integer),
        objective(varchar),
        grades(text),
        created_at(date),
        updated_at(date),
        deleted_at(date)
