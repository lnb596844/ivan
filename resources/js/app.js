import Vue from 'vue'
import store from './vuex'
import Toasted from 'vue-toasted'

window.Vue = require('vue')

require('./bootstrap')
require('./interceptors')

/**
 * Toasted
 */
Vue.use(Toasted, {
  position: 'bottom-right',
  duration: 2000,
  singleton: false,
  action: {
      icon : 'close',
      onClick : (e, toastObject) => {
          toastObject.goAway(0);
      }
  }
})

Vue.config.productionTip = false
Vue.config.devtools = true

Vue.component('Grades', require('./app/grades/components/Grades.vue'));

const app = new Vue({
  el: '#app',
  store
})
