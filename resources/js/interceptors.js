import axios from 'axios'
import store from './vuex'

axios.interceptors.response.use((response) => {
  store.dispatch('clearValidationErrors')

  Vue.toasted.show(response.data.message)

  return Promise.resolve(response)
}, (error) => {
  if (error.response.status === 422) { // Unprocessable Entity (ex: Validation Errors)
    store.dispatch('setValidationErrors', error.response.data)

    Vue.toasted.show(error.response.data.message)
  }

  if (error.response.status === 500) { // Internal Server Error (ex: Syntax Errors)
    //
  }

  return Promise.reject(error)
})
