export const getTeams = ({ commit }) => {
  return axios.get("/teams")
    .then(response => {
      commit("SET_TEAMS", response.data)

      return Promise.resolve(response)
    }).catch(error => {
      return Promise.reject(error)
    })
}

export const getTeamGrades = ({ commit }, team) => {
  if (!team) {
    return
  }

  return axios.get(`/teams/${team}`)
    .then(response => {
      commit("SET_TEAM", response.data.data)

      return Promise.resolve(response)
    }).catch(error => {
      return Promise.reject(error)
    })
}

export const saveTeamGrades = ({ commit }, payload) => {
  return axios.post(`/teams/${payload.team}`, payload)
    .then(response => {
      return Promise.resolve(response)
    }).catch(error => {
      return Promise.reject(error)
    })
}
