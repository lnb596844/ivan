import { defaultFormState } from "./defaults"
import { clone } from "lodash"

export default {
  teams: [],
  form: clone(defaultFormState)
}
