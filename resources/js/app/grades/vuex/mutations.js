import { defaultFormState } from "./defaults"
import { clone } from "lodash"

export const SET_TEAMS = (state, teams) => state.teams = teams
export const SET_TEAM = (state, team) => {
  let selectedTeam = state.form.team

  if (team.grades) {
    state.form = {
      team: team.id,
      objective: team.grades.objective,
      grades: team.grades.grades ? team.grades.grades : clone(defaultFormState.grades)
    }
  } else {
    state.form = clone(defaultFormState)
    state.form.team = selectedTeam
  }
}
