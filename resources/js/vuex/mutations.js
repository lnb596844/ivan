export const SET_VALIDATION_ERRORS = (state, errors) => state.validation = errors
export const CLEAR_VALIDATION_ERRORS = (state) => state.validation = []
