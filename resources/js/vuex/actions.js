export const setValidationErrors = ({ commit }, data) => {
  commit('SET_VALIDATION_ERRORS', data.errors)
}

export const clearValidationErrors = ({ commit }) => {
  commit('CLEAR_VALIDATION_ERRORS')
}
