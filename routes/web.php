<?php

/**
 * Home
 */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

/**
 * Teams
 */
Route::get('teams', 'TeamController@index');
Route::get('teams/{id}', 'TeamController@show');
Route::post('teams/{id}', 'TeamController@update');
